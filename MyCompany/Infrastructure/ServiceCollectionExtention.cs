﻿using Microsoft.Extensions.DependencyInjection;
using MyCompany.Serveces;
using MyCompany.Serveces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyCompany
{
    public static class ServiceCollectionExtention
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<ICourses, CorsesService>();
            services.AddScoped<IGroups, GroupsService>();
            services.AddScoped<IStudents, StudentsService>();
        }
    }
}
