﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyCompany.Models
{
    public class Course
    {
        public Guid CourseID { get; set; }

        [Required (ErrorMessage = "Необходимо ввести название курса")]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "Недопустимая длинна")]
        [Display(Name = "Название курса")]
        [RegularExpression(@"\S+",ErrorMessage = "Недопустимое название для данного поля")]
        public string CourseName { get; set; }
        public List<Group> Groups { get; set; } = new List<Group>();

    }
}
