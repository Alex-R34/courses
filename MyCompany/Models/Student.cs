﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyCompany.Models
{
    public class Student
    {
        public Guid StudentID { get; set; }
        public Guid GroupID { get; set; }

        [Required (ErrorMessage = "Это поле не может быть пустым")]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Недопустимая длинна")]
        [RegularExpression(@"\S+", ErrorMessage = "Недопустимое название для данного поля")]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Введите фамилию студента")]
        [StringLength(50,MinimumLength = 2, ErrorMessage = "Недопустимая длинна")]
        [RegularExpression(@"\S+", ErrorMessage = "Недопустимое название для данного поля")]
        [Display(Name = "Фамилия")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Это поле не может быть пустым")]
        [Range(16,70, ErrorMessage = "Вы вышли за границу допустимых значений")]
        [Display(Name = "Возраст")]
        public byte Age { get; set; }
    }
}
