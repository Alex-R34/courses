﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyCompany.Models
{
    public class Group
    {
        public Guid GroupID { get; set; }
        public Guid CourseID { get; set; }

        [Required (ErrorMessage = "Необходимо ввести название группы")]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "Недопустимая длинна")]
        [RegularExpression(@"\S+", ErrorMessage = "Недопустимое название для данного поля")]
        [Display(Name = "Название группы")]
        public string GroupName { get; set; }
        public List<Student> Students { get; set; } = new List<Student>();

    }
}
