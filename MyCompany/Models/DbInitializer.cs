﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using MyCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EFDataAccess.Models
{
    public class DbInitializer
    {
        public static void InitializeDB(CompanyContext context)
        {

            if (context.Courses.Any())
            {
                return;
            }
            // Добавляем Курсы
            context.Courses.AddRange(
                new Course
                {
                    CourseID = Guid.Parse("d7fe07e4-0c49-4a38-938f-31ac8c3518e8"),
                    CourseName = "C#",
                },
                new Course
                {
                    CourseID = Guid.Parse("3347b382-9517-4e3e-af08-fddb422ff9b6"),
                    CourseName = "C++"
                },
                new Course
                {
                    CourseID = Guid.Parse("48a8e48f-017d-4e56-befa-c41fe87d4f95"),
                    CourseName = "Java"
                },
                new Course
                {
                    CourseID = Guid.Parse("55f531f2-2379-4164-831b-139d2d8f5c1c"),
                    CourseName = "Pyton"
                },
                new Course
                {
                    CourseID = Guid.Parse("ab3f0712-42bc-4399-8e9d-2dec733401f2"),
                    CourseName = "PHP"
                },
                new Course
                {
                    CourseID = Guid.Parse("886ced7a-f935-42f0-9da3-eda39cffd4c8"),
                    CourseName = "Android"
                }

                );


            //Добавляем Группы
            context.Groups.AddRange(
                new Group
                {
                    CourseID = Guid.Parse("d7fe07e4-0c49-4a38-938f-31ac8c3518e8"),
                    GroupID = Guid.Parse("809a5e9d-a8e1-4a3c-b453-0c25acf2f295"),
                    GroupName = "C# - 001"
                },
                new Group
                {
                    CourseID = Guid.Parse("d7fe07e4-0c49-4a38-938f-31ac8c3518e8"),
                    GroupID = Guid.Parse("b17b1f2b-ee02-4e26-9b5c-a574ec609ff2"),
                    GroupName = "C# - 002"
                },
                new Group
                {
                    CourseID = Guid.Parse("d7fe07e4-0c49-4a38-938f-31ac8c3518e8"),
                    GroupID = Guid.Parse("ff0c866e-8053-4217-b82a-c3acfb7708ce"),
                    GroupName = "C# - 003"
                },

                new Group
                {
                    CourseID = Guid.Parse("3347b382-9517-4e3e-af08-fddb422ff9b6"),
                    GroupID = Guid.Parse("a0e9600f-c320-416a-b12b-0eb6b8b616b7"),
                    GroupName = "С++ - 001"
                },

                new Group
                {
                    CourseID = Guid.Parse("48a8e48f-017d-4e56-befa-c41fe87d4f95"),
                    GroupID = Guid.Parse("358601e4-4ad0-4174-827e-468ae30f9a67"),
                    GroupName = "Java - 001"

                },
                new Group
                {
                    CourseID = Guid.Parse("48a8e48f-017d-4e56-befa-c41fe87d4f95"),
                    GroupID = Guid.Parse("62a35a06-a55d-48aa-abeb-a5b380cd1d14"),
                    GroupName = "Java - 002"
                },
                new Group
                {
                    CourseID = Guid.Parse("48a8e48f-017d-4e56-befa-c41fe87d4f95"),
                    GroupID = Guid.Parse("20fc2df4-fafc-46f9-bce7-6cf4f85fa706"),
                    GroupName = "Java - 003"
                },

                new Group
                {
                    CourseID = Guid.Parse("55f531f2-2379-4164-831b-139d2d8f5c1c"),
                    GroupID = Guid.Parse("43dbb5a4-2fae-447f-98fd-b28adf66a21e"),
                    GroupName = "Pyton - 001"
                },
                new Group
                {
                    CourseID = Guid.Parse("55f531f2-2379-4164-831b-139d2d8f5c1c"),
                    GroupID = Guid.Parse("eabedc6d-c8a3-44c4-9cc5-dad8b24cc619"),
                    GroupName = "Pyton - 002"
                },

                new Group
                {
                    CourseID = Guid.Parse("ab3f0712-42bc-4399-8e9d-2dec733401f2"),
                    GroupID = Guid.Parse("6aacf857-092d-4816-bd80-f6d8a343da65"),
                    GroupName = "PHP - 001"
                },
                new Group
                {
                    CourseID = Guid.Parse("ab3f0712-42bc-4399-8e9d-2dec733401f2"),
                    GroupID = Guid.Parse("9bc7ebe7-459d-4c95-b0fd-dcdd95b0934d"),
                    GroupName = "PHP - 002"
                },

                new Group
                {
                    CourseID = Guid.Parse("886ced7a-f935-42f0-9da3-eda39cffd4c8"),
                    GroupID = Guid.Parse("73081b31-a116-4e86-9d80-6ab975d891ea"),
                    GroupName = "Andriod - 001"
                }

                );

            // Добавляем студентов
            context.Students.AddRange(
                new Student
                {
                    GroupID = Guid.Parse("809a5e9d-a8e1-4a3c-b453-0c25acf2f295"),
                    Age = 22,
                    Name = "Рик",
                    Surname = "Санчез"
                },
                new Student
                {
                    GroupID = Guid.Parse("809a5e9d-a8e1-4a3c-b453-0c25acf2f295"),
                    Age = 23,
                    Name = "Морти",
                    Surname = "Смит"
                },
                new Student
                {
                    GroupID = Guid.Parse("809a5e9d-a8e1-4a3c-b453-0c25acf2f295"),
                    Age = 24,
                    Name = "Бет",
                    Surname = "Смит"
                },
                new Student
                {
                    GroupID = Guid.Parse("809a5e9d-a8e1-4a3c-b453-0c25acf2f295"),
                    Age = 25,
                    Name = "Саммер",
                    Surname = "Смит"
                },
                new Student
                {
                    GroupID = Guid.Parse("809a5e9d-a8e1-4a3c-b453-0c25acf2f295"),
                    Age = 26,
                    Name = "Джери",
                    Surname = "Смит"
                },
                new Student
                {
                    GroupID = Guid.Parse("b17b1f2b-ee02-4e26-9b5c-a574ec609ff2"),
                    Age = 27,
                    Name = "Питер",
                    Surname = "Гриффин"
                },
                new Student
                {
                    GroupID = Guid.Parse("b17b1f2b-ee02-4e26-9b5c-a574ec609ff2"),
                    Age = 28,
                    Name = "Лоис",
                    Surname = "Гриффин"
                },
                new Student
                {
                    GroupID = Guid.Parse("b17b1f2b-ee02-4e26-9b5c-a574ec609ff2"),
                    Age = 29,
                    Name = "Мег",
                    Surname = "Гриффин"
                },
                new Student
                {
                    GroupID = Guid.Parse("b17b1f2b-ee02-4e26-9b5c-a574ec609ff2"),
                    Age = 30,
                    Name = "Брайан",
                    Surname = "Гриффин"
                },
                new Student
                {
                    GroupID = Guid.Parse("ff0c866e-8053-4217-b82a-c3acfb7708ce"),
                    Age = 21,
                    Name = "Глен",
                    Surname = "Куагмир"
                },
                new Student
                {
                    GroupID = Guid.Parse("ff0c866e-8053-4217-b82a-c3acfb7708ce"),
                    Age = 22,
                    Name = "Финн",
                    Surname = "Мёртен"
                },
                new Student
                {
                    GroupID = Guid.Parse("ff0c866e-8053-4217-b82a-c3acfb7708ce"),
                    Age = 23,
                    Name = "Джейк",
                    Surname = "Пёс"
                },

                new Student
                {
                    GroupID = Guid.Parse("a0e9600f-c320-416a-b12b-0eb6b8b616b7"),
                    Age = 24,
                    Name = "Принцесса",
                    Surname = "Жвачка"
                },

                new Student
                {
                    GroupID = Guid.Parse("358601e4-4ad0-4174-827e-468ae30f9a67"),
                    Age = 25,
                    Name = "Принцесса",
                    Surname = "Ливнерог"
                },
                new Student
                {
                    GroupID = Guid.Parse("358601e4-4ad0-4174-827e-468ae30f9a67"),
                    Age = 26,
                    Name = "Граф",
                    Surname = "Лимонохват"
                },
                new Student
                {
                    GroupID = Guid.Parse("358601e4-4ad0-4174-827e-468ae30f9a67"),
                    Age = 27,
                    Name = "Саймон",
                    Surname = "Петриков"
                },
                new Student
                {
                    GroupID = Guid.Parse("358601e4-4ad0-4174-827e-468ae30f9a67"),
                    Age = 28,
                    Name = "Хансон",
                    Surname = "Абадир"
                },
                new Student
                {
                    GroupID = Guid.Parse("358601e4-4ad0-4174-827e-468ae30f9a67"),
                    Age = 29,
                    Name = "Джеки",
                    Surname = "Чан"
                },
                new Student
                {
                    GroupID = Guid.Parse("62a35a06-a55d-48aa-abeb-a5b380cd1d14"),
                    Age = 33,
                    Name = "Сергей",
                    Surname = "Табачников"
                },
                new Student
                {
                    GroupID = Guid.Parse("62a35a06-a55d-48aa-abeb-a5b380cd1d14"),
                    Age = 18,
                    Name = "Джеймс",
                    Surname = "Хетфилд"
                },
                new Student
                {
                    GroupID = Guid.Parse("62a35a06-a55d-48aa-abeb-a5b380cd1d14"),
                    Age = 19,
                    Name = "Ларс",
                    Surname = "Ульрих"
                },
                new Student
                {
                    GroupID = Guid.Parse("20fc2df4-fafc-46f9-bce7-6cf4f85fa706"),
                    Age = 40,
                    Name = "Роберт",
                    Surname = "Трухилио"
                },
                new Student
                {
                    GroupID = Guid.Parse("20fc2df4-fafc-46f9-bce7-6cf4f85fa706"),
                    Age = 52,
                    Name = "Кирк",
                    Surname = "Хэммет"
                },
                new Student
                {
                    GroupID = Guid.Parse("20fc2df4-fafc-46f9-bce7-6cf4f85fa706"),
                    Age = 62,
                    Name = "Томми",
                    Surname = "Версетти"
                },
                new Student
                {
                    GroupID = Guid.Parse("20fc2df4-fafc-46f9-bce7-6cf4f85fa706"),
                    Age = 84,
                    Name = "Макс",
                    Surname = "Пейн"
                },
                new Student
                {
                    GroupID = Guid.Parse("43dbb5a4-2fae-447f-98fd-b28adf66a21e"),
                    Age = 23,
                    Name = "Мона",
                    Surname = "Сакс"
                },
                new Student
                {
                    GroupID = Guid.Parse("43dbb5a4-2fae-447f-98fd-b28adf66a21e"),
                    Age = 53,
                    Name = "Владимир",
                    Surname = "Лем"
                },
                new Student
                {
                    GroupID = Guid.Parse("43dbb5a4-2fae-447f-98fd-b28adf66a21e"),
                    Age = 23,
                    Name = "Фридрих",
                    Surname = "Ницше"
                },
                new Student
                {
                    GroupID = Guid.Parse("eabedc6d-c8a3-44c4-9cc5-dad8b24cc619"),
                    Age = 51,
                    Name = "Артур",
                    Surname = "Шопенгауер"
                },
                new Student
                {
                    GroupID = Guid.Parse("eabedc6d-c8a3-44c4-9cc5-dad8b24cc619"),
                    Age = 42,
                    Name = "Энтон",
                    Surname = "Бёрджес"
                },
                new Student
                {
                    GroupID = Guid.Parse("eabedc6d-c8a3-44c4-9cc5-dad8b24cc619"),
                    Age = 34,
                    Name = "Курт",
                    Surname = "Воннегут"
                },
                new Student
                {
                    GroupID = Guid.Parse("6aacf857-092d-4816-bd80-f6d8a343da65"),
                    Age = 25,
                    Name = "Александр",
                    Surname = "Грибоедов"
                },
                new Student
                {
                    GroupID = Guid.Parse("6aacf857-092d-4816-bd80-f6d8a343da65"),
                    Age = 39,
                    Name = "Фёдор",
                    Surname = "Достоевский"
                },
                new Student
                {
                    GroupID = Guid.Parse("6aacf857-092d-4816-bd80-f6d8a343da65"),
                    Age = 49,
                    Name = "Франц",
                    Surname = "Кафка"
                },
                new Student
                {
                    GroupID = Guid.Parse("6aacf857-092d-4816-bd80-f6d8a343da65"),
                    Age = 29,
                    Name = "Габриель",
                    Surname = "Гарсия-Маркез"
                },
                new Student
                {
                    GroupID = Guid.Parse("6aacf857-092d-4816-bd80-f6d8a343da65"),
                    Age = 26,
                    Name = "Мигел",
                    Surname = "де Сервантез"
                },
                new Student
                {
                    GroupID = Guid.Parse("9bc7ebe7-459d-4c95-b0fd-dcdd95b0934d"),
                    Age = 16,
                    Name = "Джек",
                    Surname = "Лондон"
                },
                new Student
                {
                    GroupID = Guid.Parse("9bc7ebe7-459d-4c95-b0fd-dcdd95b0934d"),
                    Age = 28,
                    Name = "Томас",
                    Surname = "Гоббс"
                },
                new Student
                {
                    GroupID = Guid.Parse("9bc7ebe7-459d-4c95-b0fd-dcdd95b0934d"),
                    Age = 67,
                    Name = "Рене",
                    Surname = "Декарт"
                },
                new Student
                {
                    GroupID = Guid.Parse("73081b31-a116-4e86-9d80-6ab975d891ea"),
                    Age = 73,
                    Name = "Кайл",
                    Surname = "Брофловски"
                },
                new Student
                {
                    GroupID = Guid.Parse("73081b31-a116-4e86-9d80-6ab975d891ea"),
                    Age = 34,
                    Name = "Стен",
                    Surname = "Марш"
                },
                new Student
                {
                    GroupID = Guid.Parse("73081b31-a116-4e86-9d80-6ab975d891ea"),
                    Age = 53,
                    Name = "Эрик",
                    Surname = "Картман"
                },
                new Student
                {
                    GroupID = Guid.Parse("73081b31-a116-4e86-9d80-6ab975d891ea"),
                    Age = 36,
                    Name = "Кенни",
                    Surname = "Маккормик"
                }



                );
            context.SaveChanges();
        }
    }
}
