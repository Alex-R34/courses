﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyCompany.Models;
using MyCompany.Serveces.Interfaces;

namespace MyCompany.Controllers
{
    public class GroupsController : Controller
    {
        private readonly IGroups _groups;

        public GroupsController(IGroups groups)
        {
            _groups = groups;
        }

        // GET: Groups
        public async Task<IActionResult> Index()
        {
            return View(await _groups.AllGroups());
        }

        // GET: Groups/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            ViewBag.GroupID = id;

            return View(await _groups.Details(id));
        }

        // GET: Groups/Create
        public IActionResult Create(Guid id)
        {
            ViewBag.CourseID = id;

            return View(_groups.GetNewGroup(id));
        }

        // POST: Groups/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GroupID,CourseID,GroupName")] Group @group)
        {
            if (ModelState.IsValid)
            {
                await _groups.Create(@group);
                return Redirect($"/Groups/Details/{group.GroupID}");
            }

            return View(@group);
        }

        // GET: Groups/Edit/5
        public async Task<IActionResult> Edit(Guid id)
        {
            return View(await _groups.Edit(id));
        }

        // POST: Groups/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("GroupID,CourseID,GroupName")] Group @group)
        {
            if (ModelState.IsValid)
            {
                if (await _groups.Edit(id, @group))
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return NotFound();
                }
            }
            return View(@group);
        }

        // GET: Groups/Delete/5
        public async Task<IActionResult> Delete(Guid id)
        {

            return View(await _groups.Delete(id));
        }

        // POST: Groups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var group = await _groups.GetGroup(id);
            
            if (await _groups.DeleteConfirmed(id))
            {
                return Redirect($"/Courses/Details/{group.CourseID}");
            }
            else
            {
                ModelState.AddModelError("GroupName", "Невозможно удалить группу т.к. в ней содержатся студенты");
                return View(group);
            }
            

            //return RedirectToAction(nameof(Index));
        }

        private bool GroupExists(Guid id)
        {
            return _groups.GroupExists(id);
        }
    }
}