﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyCompany.Models;
using MyCompany.Serveces.Interfaces;

namespace MyCompany.Controllers
{
    public class CoursesController : Controller
    {
        private readonly ICourses _courses;

        public CoursesController(ICourses courses)
        {
            _courses = courses;
        }

        // GET: Courses
        public async Task<IActionResult> Index()
        {
            return View(await _courses.AllCourses());
        }

        // GET: Courses/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            ViewBag.CourseID = id;

            return View(await _courses.Details(id));
        }

        // GET: Courses/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Courses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CourseID,CourseName")] Course course)
        {
            if (ModelState.IsValid)
            {

                if (await _courses.Create(course))
                {
                    return Redirect($"/Courses/Details/{course.CourseID}");
                }
                else
                {
                    return NotFound();
                }
                //return RedirectToAction(nameof(Index));
            }

            return View(course);
        }

        // GET: Courses/Edit/5
        public async Task<IActionResult> Edit(Guid id)
        {
            return View(await _courses.Edit(id));
        }

        // POST: Courses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("CourseID,CourseName")] Course course)
        {
            if (ModelState.IsValid)
            {
                if (await _courses.TryEdit(id, course))
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return NotFound();
                }
            }

            return View(course);
//Exception thrown by DbContext when it was expected that SaveChanges for an entity would result in a database update but
//in fact no rows in the database were affected. This usually indicates that the database has been concurrently updated
//such that a concurrency token that was expected to match did not actually match. Note that state entries referenced by
//this exception are not serialized due to security and accesses to the state entries after serialization will return null.
// https://docs.microsoft.com/ru-ru/ef/core/saving/concurrency
        }

        // GET: Courses/Delete/5
        public async Task<IActionResult> Delete(Guid id)
        {
            return View(await _courses.Delete(id));
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            if (await _courses.DeleteConfirmed(id))
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ModelState.AddModelError("CourseName", "Невозможно удалить курс т.к. в нём содержатся группы");
                return View(_courses.GetCourse(id));
            }

        }
    }
}
