﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyCompany.Models;
using MyCompany.Serveces.Interfaces;

namespace MyCompany.Controllers
{
    public class StudentsController : Controller
    {
        private readonly IStudents _students;

        public StudentsController(IStudents students)
        {
            _students = students;
        }

        // GET: Students
        public async Task<IActionResult> Index()
        {
            return View(await _students.GetAllStudents());
        }

        // GET: Students/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            ViewBag.GroupID = id;

            return View(await _students.Details(id));
        }

        // GET: Students/Create
        public IActionResult Create(Guid id)
        {
            ViewBag.GroupID = id;
            //Student newStudent = new Student()
            //{
            //    GroupID = id
            //};
            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StudentID,GroupID,Name,Surname,Age")] Student student)
        {
            if (ModelState.IsValid)
            {
                await _students.Create(student);

                return Redirect($"/Groups/Details/{student.GroupID}");
                //return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

        // GET: Students/Edit/5
        public async Task<IActionResult> Edit(Guid id)
        {
            return View(await _students.Edit(id));
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("StudentID,GroupID,Name,Surname,Age")] Student student)
        {
            if (ModelState.IsValid)
            {
                if (_students.StudentExists(id))
                {
                    await _students.Edit(id, student);

                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return NotFound();
                }
                
            }
            return View(student);
        }

        // GET: Students/Delete/5
        public async Task<IActionResult> Delete(Guid id)
        {
            return View(await _students.Delete(id));
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var student = await _students.FindStudent(id);
            await _students.DeleteConfirmed(id);

            return Redirect($"/Groups/Details/{student.GroupID}");
        }

        private bool StudentExists(Guid id)
        {
            return _students.StudentExists(id);
        }
    }
}
