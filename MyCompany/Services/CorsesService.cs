﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using MyCompany.Models;
using MyCompany.Serveces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyCompany.Serveces
{
    public class CorsesService : ICourses
    {
        private readonly CompanyContext _context;
        public CorsesService(CompanyContext context)
        {
            _context = context;
        }

        public Task<List<Course>> AllCourses()
        {
            return _context.Courses.ToListAsync();
        }

        public Task<List<Group>> Details(Guid id)
        {
            return _context.Groups.Where(x => x.CourseID == id).ToListAsync();
        }

        public async Task<bool> Create([Bind("CourseID,CourseName")] Course course)
        {
            try
            {
                course.CourseID = Guid.NewGuid();
                _context.Add(course);
                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<Course> Edit(Guid id)
        {
            var course = await _context.Courses.FindAsync(id);
            return course;
        }

        public async Task<bool> TryEdit(Guid id, Course course)
        {
            if (CourseExists(id))
            {
                _context.Update(course);
                await _context.SaveChangesAsync();

                return true;
            }
            else
            {
                return false;
            }

        }

        public async Task<Course> Delete(Guid id)
        {
            var course = await _context.Courses
                .FirstOrDefaultAsync(m => m.CourseID == id);

            return course;
        }

        public async Task<bool> DeleteConfirmed(Guid id)
        {
            var course = await GetCourse(id);
            var groups = await _context.Groups.Where(x => x.CourseID == id).ToListAsync();

            if (groups.Count == 0)
            {
                _context.Courses.Remove(course);
                await _context.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CourseExists(Guid id)
        {
            return _context.Courses.Any(e => e.CourseID == id);
        }

        public async Task<Course> GetCourse(Guid id)
        {
            var course = await _context.Courses.FindAsync(id);
            return course;
        }
    }
}
