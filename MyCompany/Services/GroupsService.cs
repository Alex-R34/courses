﻿using Microsoft.EntityFrameworkCore;
using MyCompany.Models;
using MyCompany.Serveces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyCompany.Serveces
{
    public class GroupsService: IGroups
    {
        private readonly CompanyContext _context;

        public GroupsService(CompanyContext context)
        {
            _context = context;
        }
        public Task<List<Group>> AllGroups()
        {
            return _context.Groups.ToListAsync();
        }
        public Task<List<Student>> Details(Guid id)
        {
            var students = _context.Students.Where(x => x.GroupID == id).ToListAsync();
            return students;
        }
        public Group GetNewGroup(Guid id)
        {
            Group newGroup = new Group()
            {
                CourseID = id
            };
            return newGroup;
        }

        public async Task<bool> Create(Group group)
        {
            group.GroupID = Guid.NewGuid();
            _context.Add(group);
            await _context.SaveChangesAsync();
            return true;
        }

        public ValueTask<Group> Edit(Guid id)
        {
            var group = _context.Groups.FindAsync(id);
            return group;
        }

        public async Task<bool> Edit(Guid id, Group group)
        {
            try
            {
                _context.Update(@group);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GroupExists(@group.GroupID))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
            
        }

        public Task<Group> Delete(Guid id)
        {
            var group = _context.Groups
                .FirstOrDefaultAsync(m => m.GroupID == id);

            return group;
        }

        public async Task<bool> DeleteConfirmed(Guid id)
        {
            var group = await _context.Groups.FindAsync(id);
            
            if (!HasStudents(id))
            {
                _context.Groups.Remove(group);
                await _context.SaveChangesAsync();

                return true;
            }
            else
            {
                return false;
            }

        }

        public bool GroupExists(Guid id)
        {
            return _context.Groups.Any(e => e.GroupID == id);
        }

        public ValueTask<Group> GetGroup(Guid id)
        {
            return  _context.Groups.FindAsync(id);
        }

        public bool HasStudents(Guid id)
        {
            return _context.Students.Any(x => x.GroupID == id);
        }

    }
}