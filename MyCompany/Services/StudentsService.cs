﻿using Microsoft.EntityFrameworkCore;
using MyCompany.Models;
using MyCompany.Serveces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyCompany.Serveces
{
    public class StudentsService: IStudents
    {
        private readonly CompanyContext _context;

        public StudentsService(CompanyContext context)
        {
            _context = context;
        }

        public Task<List<Student>> GetAllStudents()
        {
            return _context.Students.ToListAsync();
        }
        public Task<Student> Details(Guid id)
        {
            var student = _context.Students
                .FirstOrDefaultAsync(m => m.StudentID == id);

            return student;
        }

        public async Task<bool> Create(Student student)
        {
            student.StudentID = Guid.NewGuid();
            _context.Add(student);
            await _context.SaveChangesAsync();
            return true;
        }

        public async ValueTask<Student> Edit(Guid id)
        {
            var student = await _context.Students.FindAsync(id);
            return student;
        }

        public async  Task Edit(Guid id, Student student)
        {
           _context.Update(student);
           await  _context.SaveChangesAsync();

            //return Task.Run(() =>
            //{
            //    _context.Update(student);
            //    _context.SaveChangesAsync();
            //});
        }

        public Task<Student> Delete(Guid id)
        {
            return _context.Students.FirstOrDefaultAsync(m => m.StudentID == id);
        }

        public async Task<bool> DeleteConfirmed(Guid id)
        {
            var student = await FindStudent(id);
            _context.Students.Remove(student);
            await _context.SaveChangesAsync();
            return true;

            //try
            //{
            //    var student = _context.Students.FindAsync(id);
            //    var group = _context.Groups.FindAsync(student.Result.GroupID);
            //    _context.Students.Remove(student.Result);
            //    await _context.SaveChangesAsync();
            //    return true;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}

        }

        public bool StudentExists(Guid id)
        {
            return _context.Students.Any(e => e.StudentID == id);
        }
        public ValueTask<Student> FindStudent(Guid id)
        {
            return _context.Students.FindAsync(id);
        }

    }
}
