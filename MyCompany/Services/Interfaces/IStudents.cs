﻿using MyCompany.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyCompany.Serveces.Interfaces
{
    public interface IStudents
    {
        public Task<List<Student>> GetAllStudents();

        public Task<Student> Details(Guid id);

        public Task<bool> Create(Student student);

        public ValueTask<Student> Edit(Guid id);

        public Task Edit(Guid id, Student student);

        public Task<Student> Delete(Guid id);

        public Task<bool> DeleteConfirmed(Guid id);

        public bool StudentExists(Guid id);
        public ValueTask<Student> FindStudent(Guid id);
    }
}
