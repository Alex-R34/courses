﻿using MyCompany.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyCompany.Serveces.Interfaces
{
    public interface ICourses
    {
        public Task<List<Course>> AllCourses();

        public Task<List<Group>> Details(Guid id);

        public Task<bool> Create(Course course);

        public Task<Course> Edit(Guid id);

        public Task<bool> TryEdit(Guid id, Course course);

        public Task<Course> Delete(Guid id);

        public Task<bool> DeleteConfirmed(Guid id);

        public bool CourseExists(Guid id);

        public Task<Course> GetCourse(Guid id);
    }
}
