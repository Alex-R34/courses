﻿using MyCompany.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyCompany.Serveces.Interfaces
{
    public interface IGroups
    {
        public Task<List<Group>> AllGroups();

        public Task<List<Student>> Details(Guid id);

        public Group GetNewGroup(Guid id);

        public Task<bool> Create(Group group);

        public ValueTask<Group> Edit(Guid id);

        public Task<bool> Edit(Guid id, Group group);

        public Task<Group> Delete(Guid id);

        public Task<bool> DeleteConfirmed(Guid id);

        public bool GroupExists(Guid id);
        public ValueTask<Group> GetGroup(Guid id);
        public bool HasStudents(Guid id);
    }
}